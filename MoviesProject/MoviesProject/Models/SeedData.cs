﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MoviesProject.Database;

namespace MoviesProject.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ApplicationContext(
            serviceProvider.GetRequiredService<
            DbContextOptions<ApplicationContext>>()))
            {
                if (context.Movies.Any())
                {
                    return;   // DB has been seeded
                }

                context.Movies.AddRange(
                new Movie
                {
                    Title = "Бобик в тумане",
                    ReleaseDate = DateTime.Parse("1989-2-12"),
                    Genre = "Romantic Gay Action",
                    VideoLink = "https://www.youtube.com/watch?v=c282gsT1iBo&t=81s",
                    Description = "Невероятная приключенческая история о настоящей любви......"
                },

                new Movie
                {
                    Title = "Хроники Верхнего Услона ",
                    ReleaseDate = DateTime.Parse("1984-3-13"),
                    Genre = "Documental",
                    VideoLink = "https://www.youtube.com/watch?v=7QWCkH-Hi-U",
                    Description = "Все тайны алкогольных вечеринок - в этом фильме"
                },

                new Movie
                {
                    Title = "ДУ - Проспект Победы. Дорога длиною в жизнь",
                    ReleaseDate = DateTime.Parse("1986-2-23"),
                    Genre = "Drama",
                    VideoLink = "https://www.youtube.com/watch?v=2dt0md_ack8",
                    Description = "Невыдуманный рассказ о человеческой стойкости, мотивации, жизни за гранью"
                },

                new Movie
                {
                    Title = "Ильяс в кармане",
                    ReleaseDate = DateTime.Parse("1959-4-15"),
                    Genre = "Romantic Gay Comedy",
                    VideoLink = "https://www.youtube.com/watch?v=A_vQLXxrAas",
                    Description = "ДРУЖЕСКАЯ комедия про все, что всех всегда интересует"
                }
                );
                context.SaveChanges();
            }
        }
    }
}
